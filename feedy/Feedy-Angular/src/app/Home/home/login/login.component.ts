
import {Component, HostListener, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {LoginService} from '../../../Shared/Services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends DialogComponent<{}, boolean> {
  username: '';
  password: '';

  constructor(dialogService: DialogService, private loginService: LoginService) {
    super(dialogService);
  }

  login() {
    this.loginService.login(this.username, this.password).subscribe();
    this.result = true;
    this.close();
  }
  @HostListener('document:keyup', ['$event']) handleKeyUp(event) {
    if (event.keyCode === 27) {
      this.close();
    }
  }
}

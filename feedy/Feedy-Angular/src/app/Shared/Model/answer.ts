import {Question} from './question';

export class Answer {
  id = 0;
  text: string;
  question: Question;
}

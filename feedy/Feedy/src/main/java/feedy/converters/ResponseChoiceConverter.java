package feedy.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import feedy.domain.ResponseChoice;
import feedy.dtos.ResponseChoiceDto;

@Component
public class ResponseChoiceConverter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public ResponseChoiceDto convertToDto(ResponseChoice responseChoice) {
		return new ResponseChoiceDto(responseChoice.getId(), responseChoice.getText());
	}

	public ResponseChoice convertFromDto(ResponseChoiceDto responseChoiceDto) {
		logger.info("convertFromDto called:{}", responseChoiceDto);

		return new ResponseChoice(responseChoiceDto.getResponseChoiceText());
	}
}

package feedy.bootstrap;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import feedy.domain.Question;
import feedy.domain.QuestionType;
import feedy.domain.ResponseChoice;
import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.Template;
import feedy.domain.User;
import feedy.repositories.QuestionTypeRepository;
import feedy.repositories.RoleRepository;
import feedy.repositories.TeamRepository;
import feedy.repositories.TemplateRepository;
import feedy.repositories.UserRepository;

@Component
public class Loader implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TemplateRepository templateRepository;

	@Autowired
	private QuestionTypeRepository questionTypeRepository;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {

		////////////////// QuestionTypes

		QuestionType singleChoiceQuestionType = new QuestionType("SINGLE_CHOICE");
		QuestionType multipleChoiceQuestionType = new QuestionType("MULTIPLE_CHOICE");
		QuestionType freetextQuestionType = new QuestionType("FREETEXT");

		questionTypeRepository.save(singleChoiceQuestionType);
		questionTypeRepository.save(multipleChoiceQuestionType);
		questionTypeRepository.save(freetextQuestionType);

		///////////////////// TEAM,ROLE

		Team java = new Team("Java", null);
		Team dotNET = new Team(".NET", null);

		Role userRole = new Role("Normal");
		Role sulRole = new Role("SUL");

		roleRepository.save(userRole);
		roleRepository.save(sulRole);
		teamRepository.save(java);
		teamRepository.save(dotNET);

		///////////// TEMPLATES

		Template templateExample = new Template("BasicQuestions", "Jhon Smith", null);
		Template templateSecondExample = new Template("QuestionsSUL", "Hannah Silver", null);
		templateRepository.save(templateExample);
		templateRepository.save(templateSecondExample);

		//////////// ResponseChoices

		///////////////// single1

		ResponseChoice responseChoiceSingleChoiceQuestion11 = new ResponseChoice("bad");
		ResponseChoice responseChoiceSingleChoiceQuestion12 = new ResponseChoice("ok");
		ResponseChoice responseChoiceSingleChoiceQuestion13 = new ResponseChoice("good");
		ResponseChoice responseChoiceSingleChoiceQuestion14 = new ResponseChoice("excellent");

		Set<ResponseChoice> respChoiceSetSingle1 = new HashSet<>();
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion11);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion12);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion13);
		respChoiceSetSingle1.add(responseChoiceSingleChoiceQuestion14);

		///////////////////////// multiple1
		ResponseChoice responseChoiceMultipleChoiceQuestion11 = new ResponseChoice("sociable");
		ResponseChoice responseChoiceMultipleChoiceQuestion12 = new ResponseChoice("good listener");
		ResponseChoice responseChoiceMultipleChoiceQuestion13 = new ResponseChoice("optimistic");
		ResponseChoice responseChoiceMultipleChoiceQuestion14 = new ResponseChoice("always in time");

		Set<ResponseChoice> respChoiceSetMultiple1 = new HashSet<>();
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion11);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion12);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion13);
		respChoiceSetMultiple1.add(responseChoiceMultipleChoiceQuestion14);

		/////////// single2

		ResponseChoice responseChoiceSingleChoiceQuestion21 = new ResponseChoice("never");
		ResponseChoice responseChoiceSingleChoiceQuestion22 = new ResponseChoice("sometimes");
		ResponseChoice responseChoiceSingleChoiceQuestion23 = new ResponseChoice("often");
		ResponseChoice responseChoiceSingleChoiceQuestion24 = new ResponseChoice("almost all the time");

		Set<ResponseChoice> respChoiceSetSingle2 = new HashSet<>();
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion21);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion22);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion23);
		respChoiceSetSingle2.add(responseChoiceSingleChoiceQuestion24);

		////////////// multiple2

		ResponseChoice responseChoiceMultipleChoiceQuestion21 = new ResponseChoice("gym");
		ResponseChoice responseChoiceMultipleChoiceQuestion22 = new ResponseChoice("football");
		ResponseChoice responseChoiceMultipleChoiceQuestion23 = new ResponseChoice("photography");
		ResponseChoice responseChoiceMultipleChoiceQuestion24 = new ResponseChoice("movies");

		Set<ResponseChoice> respChoiceSetMultiple2 = new HashSet<>();
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion21);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion22);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion23);
		respChoiceSetMultiple2.add(responseChoiceMultipleChoiceQuestion24);

		////////// QUESTIONS

		Question firstQuestion = new Question(templateExample, "Is he/she a sociable person?", freetextQuestionType,
				null, null);

		Question singleChoiceQuestion1 = new Question(templateExample, "how good is he/she at cooking?",
				singleChoiceQuestionType, respChoiceSetSingle1, null);
		Question multipleChoiceQuestion1 = new Question(templateExample, "what are their skills at work?",
				multipleChoiceQuestionType, respChoiceSetMultiple1, null);

		Question question1 = new Question(templateSecondExample, "Describe their attitude at work.",
				freetextQuestionType, null, null);
		Question singleChoiceQuestion2 = new Question(templateSecondExample, "Is he/she late for work?",
				singleChoiceQuestionType, respChoiceSetSingle2, null);
		Question multipleChoiceQuestion2 = new Question(templateSecondExample, "what are their hobbies?",
				multipleChoiceQuestionType, respChoiceSetMultiple2, null);

		Set<Question> questionSet = new HashSet<>();
		questionSet.add(firstQuestion);

		questionSet.add(singleChoiceQuestion1);
		questionSet.add(multipleChoiceQuestion1);

		Set<Question> questionSet1 = new HashSet<>();
		questionSet1.add(question1);
		questionSet1.add(singleChoiceQuestion2);
		questionSet1.add(multipleChoiceQuestion2);

		templateExample.setQuestions(questionSet);
		templateRepository.save(templateExample);

		templateSecondExample.setQuestions(questionSet1);
		templateRepository.save(templateSecondExample);

		//////////////////// USERS

		User javaUserSUL = new User("ERNI\\gaad", "admin", sulRole, java);
		userRepository.save(javaUserSUL);

		User javaUserNormal1 = new User("George McCollin", "123456", userRole, java);
		userRepository.save(javaUserNormal1);

		User javaUserNormal2 = new User("Hannah Silver", "000000", userRole, java);
		userRepository.save(javaUserNormal2);

		User javaUserNormal3 = new User("Alaric Saltzman", "111111", userRole, java);
		userRepository.save(javaUserNormal3);

		User dotNETUserSUL = new User("Debbie Frost", "222222", sulRole, dotNET);
		userRepository.save(dotNETUserSUL);

		User dotNETUserNormal1 = new User("Rebecca Beckam", "333333", userRole, dotNET);
		userRepository.save(dotNETUserNormal1);

		User dotNETUserNormal2 = new User("David Fisch", "444444", userRole, dotNET);
		userRepository.save(dotNETUserNormal2);

	}

}

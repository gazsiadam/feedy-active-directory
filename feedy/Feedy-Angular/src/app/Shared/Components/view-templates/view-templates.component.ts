import { Component, OnInit } from '@angular/core';
import {TemplateService} from '../../Services/template.service';
import {Template} from '../../Model/template';
import {LoginService} from '../../Services/login.service';
import {DialogService} from 'ng2-bootstrap-modal';
import {TemplateModalComponent} from './template-modal/template-modal.component';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-view-templates',
  templateUrl: './view-templates.component.html',
  styleUrls: ['./view-templates.component.css']
})
export class ViewTemplatesComponent implements OnInit {
  templates: Template[] = [];

  constructor(private templateService: TemplateService, private loginService: LoginService, private dialogService: DialogService
, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.setTemplates();
  }

  setTemplates() {
    this.templateService.getTemplates().subscribe(templates => this.templates = templates);
  }

  ViewTemplate(template: Template) {
    let disposable = this.dialogService.addDialog(TemplateModalComponent, {
      template: template
    });
  }
}

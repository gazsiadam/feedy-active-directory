package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Answer extends BaseEntity {

	private String text;

	@ManyToOne(fetch = FetchType.LAZY)
	private Question question;

	@ManyToOne(fetch = FetchType.LAZY)
	private Feedback feedback;

	public Answer(String text, Question question) {
		this.text = text;
		this.question = question;

	}

	public String toString() {
		return "{Answer:" + text + " " + getId() + "}";
	}

}

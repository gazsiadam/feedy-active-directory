package feedy.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.converters.UserConverter;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.dtos.TeamDto;
import feedy.dtos.UserDto;
import feedy.services.TeamService;
import feedy.services.UserService;

@RestController
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;
	@Autowired
	private TeamService teamService;
	@Autowired
	private UserConverter userConverter;

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/usersByTeam", method = RequestMethod.GET)
	public List<TeamDto> getUsersByTeam() {
		logger.info("UsersByTeam called");

		ArrayList<TeamDto> teamList = (ArrayList<TeamDto>) teamService.listAll().stream().map(t -> (Team) t)
				.map(t1 -> new TeamDto(t1.getName(), new ArrayList<>())).collect(Collectors.toList());
		ArrayList<UserDto> userList = (ArrayList<UserDto>) userService.listAll().stream().map(u -> (User) u)
				.map(u1 -> userConverter.convertToDto(u1)).collect(Collectors.toList());

		for (UserDto userDto : userList)
			for (TeamDto teamDto : teamList) {
				if (teamDto.getTeamName().equals(userDto.getTeam()))
					teamDto.addUserToList(userDto);
			}

		logger.info("result:{}", teamList);
		return teamList;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/deleteUser/{userId}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable int userId) {
		logger.info("deleteUser called: userid:{}", userId);

		userService.delete(userId);
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public void saveUser(@RequestBody UserDto userDto) {
		logger.info("addUser called:{}", userDto);

		User user = userConverter.convertFromDto(userDto);

		logger.info("addUser result:{}", userService.saveOrUpdate(user));

	}

}

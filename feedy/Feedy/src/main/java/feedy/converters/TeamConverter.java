package feedy.converters;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Team;
import feedy.dtos.TeamDto;
import feedy.services.TeamService;

@Component
public class TeamConverter {

	@Autowired
	private static TeamService teamService;

	public static TeamDto convertToDto(Team team) {
		return new TeamDto(team.getName(), new ArrayList<>());
	}

	public static Team convertFromDto(TeamDto teamDto) {
		return teamService.findByName(teamDto.getTeamName());
	}
}

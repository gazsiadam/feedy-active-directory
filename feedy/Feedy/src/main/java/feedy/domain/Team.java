package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Team extends BaseEntity {

	private String name;
	@OneToOne
	private User sul;

	public Team() {
		super();
	}

	public Team(String name, @Nullable User sul) {
		this.name = name;
		this.sul = sul;
	}

	public String toString() {
		return "{Team:" + name + " " + getId() + "}";
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Team))
			return false;
		if (obj == this)
			return true;
		Team team = (Team) obj;
		if (team.getName().equals(this.name)) {
			if (sul != null) {
				return (team.getSul().equals(this.sul));
			} else
				return true;
		}

		return false;
	}

	public int hashCode() {
		return id;
	}
}

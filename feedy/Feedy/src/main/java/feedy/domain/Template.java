package feedy.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Template extends BaseEntity {

	private String name;
	private String author;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<Question> questions;

	public Template() {
	}

	public Template(String name, String author, @Nullable Set<Question> questions) {

		this.name = name;
		this.author = author;
		this.questions = questions;
	}

	public String toString() {
		return "{Template:" + name + " " + author + " " + getId() + "}";
	}

}

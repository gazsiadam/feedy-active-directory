package feedy.services;

import feedy.domain.Team;

public interface TeamService extends CRUDService<Team> {
	Team findByName(String teamName);
}

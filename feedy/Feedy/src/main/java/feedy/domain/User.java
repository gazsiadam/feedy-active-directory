package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class User extends BaseEntity {

	private String name;
	private String password;

	@OneToOne
	private Role role;

	@OneToOne
	private Team team;

	public User() {
	}

	public User(String name, String password, Role role, @Nullable Team team) {

		this.name = name;
		this.password = password;
		this.role = role;
		this.team = team;
	}

	public User(int id, String name, String password, Role role, @Nullable Team team) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.role = role;
		this.team = team;
	}

	public String toString() {
		return "{User:" + name + " " + password + " " + role + " " + team + " " + getId() + "}";
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof User))
			return false;
		if (obj == this)
			return true;
		User user = (User) obj;
		return this.name.equals(user.getName()) && this.password.equals(user.getPassword())
				&& this.team.equals(user.getTeam()) && this.role.equals(user.getRole());

	}

	public int hashCode() {
		return id;
	}
}

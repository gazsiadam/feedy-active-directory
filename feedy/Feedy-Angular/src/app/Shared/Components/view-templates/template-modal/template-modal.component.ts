import { Component, OnInit } from '@angular/core';

import {Question} from '../../../Model/question';

import {Template} from '../../../Model/template';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';


export interface TemplateModal {
  template: Template;
}

@Component({
  selector: 'app-template-modal',
  templateUrl: './template-modal.component.html',
  styleUrls: ['./template-modal.component.css']
})
export class TemplateModalComponent extends DialogComponent<TemplateModal, boolean> implements TemplateModal, OnInit {
  template: Template;
  questions: Question[] = [];
  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() {
    this.getQuestions();
    console.log(this.template);
  }

  formatQuestions(questions: Question[]) {
    for (let i = 0; i < questions.length; i++) {
      questions[i].questionText = questions[i].questionText[0].toUpperCase() + questions[i].questionText.slice(1);
    }
  }

  private getQuestions() {
     this.questions = this.template.questions;
    this.formatQuestions(this.questions);
  }
}

import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {BaseURL} from '../BaseURL';
import {HeaderBuilder} from '../header-builder';
import {LoginService} from './login.service';
import {Feedback} from '../Model/feedback';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class FeedbackService {

  private recivedFeedbacksURL = BaseURL.getURL() + '/getReceivedFeedbacks';
  private sentFeedbacksURL = BaseURL.getURL() + '/getSentFeedbacks';
  private saveFeedbackURL = BaseURL.getURL() + '/saveFeedback';
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
  }

  public getFeedbacksRecived(username: string): Observable<Feedback[]> {
    let url = this.recivedFeedbacksURL + '/' + username;
    return this.http.get(url, this.options).map(res => res.json() as Feedback[]);
  }

  public getFeedbacksSent(username: string): Observable<Feedback[]> {
    let url = this.sentFeedbacksURL + '/' + username;
    return this.http.get(url, this.options).map(res => res.json() as Feedback[]);
  }

  public saveFeedback(Feedback: Feedback) {
    return this.http.post(this.saveFeedbackURL, JSON.stringify(Feedback), this.options);
  }

}

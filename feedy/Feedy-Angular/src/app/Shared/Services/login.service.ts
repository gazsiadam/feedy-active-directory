import {Injectable, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {BaseURL} from '../BaseURL';
import {Router} from "@angular/router";

@Injectable()
export class LoginService implements OnInit {
  private loginUrl = BaseURL.getURL() + '/login';
  private logoutUrl = BaseURL.getURL() + '/logout';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http, private router: Router) {
  }

  ngOnInit() {
  }

  login(username: string, password: string) {
    let _sub = this.http.post(this.loginUrl, JSON.stringify({username: username, password: password}))
      .map((response: Response) => {
        let token = response.json()['Token'];
        let role = response.json()['Role'];
        if (token) {
          sessionStorage.setItem('currentUser', JSON.stringify({username: username, token: token, role: role}));
          return true;
        } else {
          return false;
        }
      }).catch((error: any) => {
        alert('Bad Credential');
        return Observable.throw(error.json().error || 'Server error')
      });
    this.router.navigate(['/colleagues']);
    return _sub;
  }

  getToken(): string {
    let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    let token = currentUser && currentUser.token;
    return token ? token : '';
  }

  logout(): void {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    sessionStorage.removeItem('currentUser');
    this.http.post(this.logoutUrl, {}, headers).catch((error: any) => {
      return Observable.throw(error || 'Server error');
    }).subscribe();
    this.router.navigate(['/']);
  }

  isLoggedIn(): boolean {
    let token: String = this.getToken();
    return token && token.length > 0;
  }

  getUser() {
    if (sessionStorage.getItem('currentUser') == null) {
      return '';
    }
    return JSON.parse(sessionStorage.getItem('currentUser'))['username'];
  }

  public getRole(): string {
    if (sessionStorage.getItem('currentUser') == null) {
      return '';
    }
    return JSON.parse(sessionStorage.getItem('currentUser'))['role'];
  }
}

import { Component, OnInit} from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {Template} from '../../Shared/Model/template';
import {TemplateService} from '../../Shared/Services/template.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';


@Component({
  selector: 'app-template-generator',
  templateUrl: './template-generator.component.html',
  styleUrls: ['./template-generator.component.css']
})

export class TemplateGeneratorComponent implements OnInit {

  public myForm: FormGroup;
  public questionTypes: string[];
  public defaultType: string;

  constructor(private _fb: FormBuilder, private templateService: TemplateService, private toastr: ToastrService, private router: Router) {
    this.questionTypes = ['SINGLE_CHOICE', 'MULTIPLE_CHOICE'];
    this.defaultType = 'FREETEXT';
  }

  ngOnInit() {
    this.myForm = this._fb.group({
      name:  ['', [Validators.required, Validators.minLength(5)]],
      questions: this._fb.array([])
    });
    this.addQuestion();
  }

  clearArray(formArray: FormArray) {
    let length = formArray.length;
    while (length > 0) {
      formArray.removeAt(length - 1);
      length = length - 1;
    }
    formArray.removeAt(0);
  }

  addQuestion() {
    const questionControl = <FormArray>this.myForm.controls['questions'];
    let question = this.initQuestion();
    question['controls']['questionType']['valueChanges'].subscribe(val => {
      if (question['controls']['questionType']['value'] === 'FREETEXT') {
        this.clearArray(<FormArray>question['controls']['responseChoices']);
      }
    });

    questionControl.push(question);
  }

  removeQuestion(i: number) {
    const questionControl = <FormArray>this.myForm.controls['questions'];
    questionControl.removeAt(i);
  }

  initQuestion() {
    return this._fb.group({
      questionText: ['', Validators.required],
      questionType: [this.defaultType, [Validators.required]],
      responseChoices: this._fb.array([])
    });
  }

  initAnswer() {
    return this._fb.group({
      responseChoiceText: ['', Validators.required],
    });
  }

  removeAnswer(i: number, j: number) {
    const answerControl = this.myForm.controls.questions['controls'][i]['controls'].responseChoices;
    answerControl.removeAt(j);
  }

  addAnswer(i: number) {
    let answerControl = this.myForm.controls.questions['controls'][i]['controls'].responseChoices;
    let answer = this.initAnswer();
    answerControl.push(answer);
  }

  getAnswers(i) {
    return this.myForm.controls.questions['controls'][i]['controls'].responseChoices.controls;
  }

  getQuestionType(i) {
    let value = this.myForm.controls.questions['controls'][i]['controls'].questionType._value;
    if (value === '') {
      return this.defaultType;
    }
    return value;
  }

  successToast() {
    this.toastr.success('Template Added!', 'Success!');
  }

  failureToast() {
    this.toastr.error('Some error occured!', 'Error!');
  }

  validateTempalte(template: Template): boolean {
    let i, j;
    let valid = true;
    for ( i = 0; i < template.questions.length; i++) {
      if (template.questions[i].questionText === '') {
          valid = false;
      }
      console.log(template.questions[i]);
      for ( j = 0; j < template.questions[i].responseChoices.length; j++) {
        if ( template.questions[i].responseChoices[j].responseChoiceText === '') {
          valid = false;
        }
      }
    }
    return valid;
  }

  save(template: Template) {
      if (this.validateTempalte(template) === true) {
        let noError = true;
        this.templateService.saveTemplate(template).subscribe(res => {
          this.successToast();
          this.clearArray(<FormArray>this.myForm.controls['questions']);
          this.myForm.reset();

          this.ngOnInit();

          },
          err => {
            this.failureToast()
        });
      }
  }
}


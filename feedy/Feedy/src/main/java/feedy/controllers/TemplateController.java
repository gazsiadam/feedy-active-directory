package feedy.controllers;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import exception.DuplicateTemplateNameException;
import feedy.converters.QuestionConverter;
import feedy.converters.TemplateConverter;
import feedy.domain.Question;
import feedy.domain.Template;
import feedy.dtos.QuestionDto;
import feedy.dtos.TemplateDto;
import feedy.services.QuestionService;
import feedy.services.TemplateService;

@RestController
public class TemplateController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TemplateService templateService;

	@Autowired
	private TemplateConverter templateConverter;

	@Autowired
	private QuestionConverter questionConverter;
	
	@Autowired
	private QuestionService questionService;

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/templates", method = RequestMethod.GET)
	public List<TemplateDto> getTemplates() {
		logger.info("getTemplates called");
		List<TemplateDto> templates = templateService.listAll().stream().map(t -> (Template) t)
				.map(t -> templateConverter.convertToDto(t)).collect(Collectors.toList());
		logger.info("getTemplates result:{}", templates);
		return templates;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/saveTemplate", method = RequestMethod.POST)
	public TemplateDto save(@RequestBody TemplateDto templateDto) throws DuplicateTemplateNameException {
		logger.info("addTemplate called:{}", templateDto);

		Template template = templateConverter.convertFromDto(templateDto);

		if (templateService.findByName(template.getName()) == null) {
			logger.info("addTemplate result:{}", templateService.saveOrUpdate(template));
			template.getQuestions().stream().forEach(question -> {

				question.setTemplate(template);
				questionService.saveOrUpdate(question);
				if (question.getAnswers() == null) {
					return;
				}
				if (question.getAnswers().isEmpty() && !"FREETEXT".equals(question.getQuestionType().getName())) {
					logger.info("Invalid question(only freetext can have empty array for response choices):{}",
							question);
					template.getQuestions().remove(question);
				}
				if (!"FREETEXT".equals(question.getQuestionType().getName())) {
					question.getResponseChoices().stream().forEach(response -> response.setQuestion(question));
				}
			});
			return templateDto;
		} else {
			logger.info("addTemplate result: A template with the same name already exists !");
			throw new DuplicateTemplateNameException("Duplicate template name not allowed");
		}

	}

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/getQuestionsByTemplate/{templateId}", method = RequestMethod.GET)
	public Set<QuestionDto> getQuestionsByTemplate(@PathVariable int templateId) {
		Set<Question> questions;
		logger.info("Template id:{}", templateId);
		Set<QuestionDto> questionsDto = new TreeSet<>(Comparator.comparing(QuestionDto::getId));
		Template template = templateService.getById(templateId);
		logger.info("Template:{}", template);
		if (template != null) {
			try {
				logger.info("Template:{}", template);
				questions = template.getQuestions();
				for (Question question : questions) {
					logger.info("Question:{}", question);
					QuestionDto questionDto = questionConverter.convertToDto(question);
					questionsDto.add(questionDto);
					logger.info("QuestionDto:{}", questionDto);
				}

			} catch (NullPointerException e) {
				logger.info("Null question list in template \n get Questions of template ID:{}", templateId);
			}
		}
		logger.info("getQuestionsByTemplate result:{}", questionsDto);
		return questionsDto;
	}

	@PreAuthorize("hasRole('SUL')")
	@RequestMapping(value = "/templateByName/{name}", method = RequestMethod.GET)
	public Template getTemplateByName(@PathVariable String name) {
		Template template = templateService.findByName(name);
		if (template == null)
			return new Template();
		else
			return template;

	}

}

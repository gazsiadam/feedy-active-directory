package feedy.converters;

import java.sql.Date;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Answer;
import feedy.domain.Feedback;
import feedy.dtos.AnswerDto;
import feedy.dtos.FeedbackDto;
import feedy.services.UserService;

@Component
public class FeedbackConverter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AnswerConverter answerConverter;
	@Autowired
	private UserService userService;

	public FeedbackDto convertToDto(Feedback feedback) {
		logger.info("convertToDto called:{}", feedback);
		Set<AnswerDto> answers = feedback.getAnswers().stream().map(a -> answerConverter.convertToDto(a))
				.collect(Collectors.toSet());
		FeedbackDto feedbackDto = new FeedbackDto(feedback.getFrom().getName(), feedback.getTo().getName(),
				feedback.isAnonymous(), feedback.getDate().toString(), answers);
		logger.info("convertToDto result:{}", feedbackDto);
		return feedbackDto;
	}

	public Feedback convertFromDto(FeedbackDto feedbackDto) {
		logger.info("convertFromDto called:{}", feedbackDto);

		Set<Answer> answers = feedbackDto.getAnswers().stream().map(a -> answerConverter.convertFromDto(a))
				.collect(Collectors.toSet());
		Date date = new Date(Long.parseLong(feedbackDto.getDate().replace(".", "")));
		logger.info("blah {}", date.toString());
		Feedback feedback = new Feedback(userService.findByName(feedbackDto.getFrom()),
				userService.findByName(feedbackDto.getTo()), feedbackDto.isAnonymous(), date, answers);

		logger.info("convertFromDto result:{}", feedback);
		return feedback;
	}

}

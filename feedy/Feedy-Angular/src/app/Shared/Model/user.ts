export class User {
  id: number;
  name: string;
  team: string;
  role: string;
  password = '';

  constructor(name: string, team: string, role: string, password: string) {
    this.id = 0;
    this.name = name;
    this.team = team;
    this.role = role;
    this.password = password;
  }
}

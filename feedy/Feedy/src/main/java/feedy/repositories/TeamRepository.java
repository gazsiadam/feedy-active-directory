package feedy.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {
	public List<Team> findByName(String name);
}

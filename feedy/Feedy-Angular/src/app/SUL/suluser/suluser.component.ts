import {LoginService} from '../../Shared/Services/login.service';
import {Component, OnInit} from '@angular/core';
import {DialogService} from 'ng2-bootstrap-modal';
import {ModalComponent} from '../../Shared/Components/modal/modal.component'

@Component({
  selector: 'app-suluser',
  templateUrl: './suluser.component.html',
  styleUrls: ['./suluser.component.css']
})
export class SuluserComponent implements OnInit {
  userName: string;

  constructor(private loginService: LoginService, private dialogService: DialogService) {
  }

  ngOnInit() {
    this.setName();
  }

  setName() {
    this.userName = this.loginService.getUser();
    if (this.userName === '') {
      alert('Unauthorized!');
      this.loginService.logout();
    }
  }


  logout() {
    this.loginService.logout();
  }

  showAddPopup() {
    let disposable = this.dialogService.addDialog(ModalComponent, {
      title: 'Add User',
      user: undefined
    });
  }


}


import {LoginService} from './Shared/Services/login.service';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HomeComponent} from './Home/home/home.component';
import {BasicuserComponent} from './NormalUser/basicuser/basicuser.component';
import {SuluserComponent} from './SUL/suluser/suluser.component';
import {AppRoutingModule} from './app.routing';
import {HttpModule} from '@angular/http';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import {LoginComponent} from './Home/home/login/login.component';
import {UsersService} from './Shared/Services/users.service';
import {ColleaguesComponent} from './Shared/Components/colleagues/colleagues.component';
import {ExpandableListModule} from 'angular2-expandable-list';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderBuilder} from './Shared/header-builder';
import {RoleService} from './Shared/Services/role.service';
import {TeamService} from './Shared/Services/team.service';
import {ReportService} from './Shared/Services/report.service';
import {ModalComponent} from './Shared/Components/modal/modal.component';
import { TemplateGeneratorComponent } from './SUL/template-generator/template-generator.component';
import { ReportGeneratorComponent } from './SUL/report-generator/report-generator.component'
import {TemplateService} from './Shared/Services/template.service';
import { UserPipe } from './Shared/Components/colleagues/user.pipe';
import {FeedbackComponent} from './Shared/Components/feedback/feedback.component';
import {QuestionService} from './Shared/Services/question.service';
import { ViewFeedbacksComponent } from './Shared/Components/view-feedbacks/view-feedbacks.component';
import {FeedbackService} from './Shared/Services/feedback.service';
import { FeedbackModalComponent } from './Shared/Components/view-feedbacks/feedback-modal/feedback-modal.component';
import {ToastrModule} from 'ngx-toastr';
import { ViewTemplatesComponent } from './Shared/Components/view-templates/view-templates.component';
import { TemplateModalComponent } from './Shared/Components/view-templates/template-modal/template-modal.component';
import {MdButtonModule, MdCheckboxModule} from '@angular/material';
import {MdTabsModule} from '@angular/material';
import 'hammerjs';

import {ChartsModule} from "ng2-charts";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BasicuserComponent,
    SuluserComponent,
    LoginComponent,
    ColleaguesComponent,
    ModalComponent,
    TemplateGeneratorComponent,
    UserPipe,
    FeedbackComponent,
    ViewFeedbacksComponent,
    FeedbackModalComponent,
    ViewTemplatesComponent,
    TemplateModalComponent,
    ReportGeneratorComponent
  ],
  imports: [
    MdButtonModule,
    ChartsModule,
    MdCheckboxModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BootstrapModalModule,
    BrowserAnimationsModule,
    ExpandableListModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    MdTabsModule
  ],
  entryComponents: [
    LoginComponent, ModalComponent, FeedbackModalComponent, TemplateModalComponent
  ],

    providers: [LoginService, UsersService, HeaderBuilder, RoleService, TeamService, TemplateService, QuestionService,
    FeedbackService, ReportService],

  bootstrap: [AppComponent],

})
export class AppModule {}

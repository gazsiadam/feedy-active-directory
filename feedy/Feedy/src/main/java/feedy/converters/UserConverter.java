package feedy.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Role;
import feedy.domain.Team;
import feedy.domain.User;
import feedy.dtos.UserDto;
import feedy.services.RoleService;
import feedy.services.TeamService;
import feedy.services.UserService;

@Component
public class UserConverter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private TeamService teamService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;

	public UserDto convertToDto(User user) {
		logger.info("convertToDto called {}", user);
		UserDto userDto = new UserDto(user.getId(), user.getName(), user.getTeam().getName(), user.getRole().getName());

		logger.info("convertToDto result {}", userDto);
		return userDto;
	}

	public User convertFromDto(UserDto userDto) {
		logger.info("convertFromDto called:{}", userDto);
		Team userTeam = teamService.findByName(userDto.getTeam());
		Role userRole = roleService.findByName(userDto.getRole());
		String password = null;
		User user = null;

		if (userDto.getId() != 0) {
			password = userService.getById(userDto.getId()).getPassword();
			user = new User(userDto.getId(), userDto.getName(), password, userRole, userTeam);
		} else {
			password = userDto.getPassword();
			user = new User(userDto.getName(), password, userRole, userTeam);
		}

		logger.info("convertFromDto result:{}", user);
		return user;
	}

}

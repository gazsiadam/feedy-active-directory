package feedy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer> {

}

package feedy.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.Question;
@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer>{

}

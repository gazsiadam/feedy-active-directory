package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Role;
import feedy.repositories.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {

	private RoleRepository roleRepository;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	public void setRoleRepository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public List<Role> listAll() {
		logger.info("listAll called");

		List<Role> roles = roleRepository.findAll();

		logger.info("listAll result{}", roles);
		return roles;
	}

	@Override
	public Role getById(Integer id) {
		logger.info("getById called {}", id);

		Role role = roleRepository.findOne(id);

		logger.info("getById", role);
		return role;
	}

	@Override
	public Role saveOrUpdate(Role domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return roleRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		roleRepository.delete(id);
	}

	@Override
	public Role findByName(String name) {
		logger.info("findByName called:{}", name);

		Role role = roleRepository.findByName(name).get(0);

		logger.info("findByName result:{}", role);
		return role;
	}

}

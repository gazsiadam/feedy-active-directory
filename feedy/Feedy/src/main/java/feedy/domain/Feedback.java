package feedy.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity

public class Feedback extends BaseEntity {

	@OneToOne
	private User from;
	@OneToOne
	private User to;
	private boolean anonymous;
	private Date date;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<Answer> answers;

	public Feedback() {

	}

	public Feedback(User from, User to, boolean anonymous, Date date, Set<Answer> answers) {
		this.from = from;
		this.to = to;
		this.anonymous = anonymous;
		this.date = date;
		this.answers = answers;
	}

	public String toString() {
		return "{Feedback:" + from + " " + to + " " + isAnonymous() + " " + getDate() + " " + getId() + "}";
	}

}

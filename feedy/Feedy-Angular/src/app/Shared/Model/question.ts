import {ResponseChoice} from './ResponseChoice';

export class Question {
  id = 0;
  questionText: string;
  questionType: string;
  responseChoices: ResponseChoice[];
}

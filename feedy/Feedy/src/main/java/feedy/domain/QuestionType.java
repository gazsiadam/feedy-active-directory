package feedy.domain;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class QuestionType extends BaseEntity {

	private String name;
	public QuestionType() {
	}

	public QuestionType(String name) {
		this.name = name;
	}

	public String toString() {
		return "{QuestionType:" + name + " " + getId() + "}";
	}

}

package feedy.controllers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.converters.FeedbackConverter;
import feedy.domain.Feedback;
import feedy.domain.User;
import feedy.dtos.AnswerDto;
import feedy.dtos.FeedbackDto;
import feedy.services.AnswerService;
import feedy.services.FeedbackService;
import feedy.services.UserService;

@RestController
public class FeedbackController {
	private List<AnswerDto> answers;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private AnswerService answerService;

	@Autowired
	private FeedbackConverter feedbackConverter;

	public void addAnswerToFeedback(AnswerDto answerDto) {
		answers.add(answerDto);
	}

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/getSentFeedbacks/{from}", method = RequestMethod.GET)
	public Set<FeedbackDto> getSentFeedbacks(@PathVariable String from) {

		User user = userService.findByName(from);
		Set<Feedback> feedbacks = feedbackService.findByFrom(user);
		return feedbacks.stream().map(fDto -> feedbackConverter.convertToDto(fDto))
				.collect(Collectors.toSet());

	}

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/getReceivedFeedbacks/{to}", method = RequestMethod.GET)
	public Set<FeedbackDto> getReceivedFeedbacks(@PathVariable String to) {

		User user = userService.findByName(to);
		Set<Feedback> feedbacks = feedbackService.findByTo(user);
		return feedbacks.stream().map(fDto -> feedbackConverter.convertToDto(fDto))
				.collect(Collectors.toSet());

	}

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/saveFeedback", method = RequestMethod.POST)
	public void saveFeedback(@RequestBody FeedbackDto feedbackDto) {
		logger.info("addFeedback called:{}", feedbackDto);

		Feedback feedback = feedbackConverter.convertFromDto(feedbackDto);
		
		Feedback feedbackOther = feedbackService.saveOrUpdate(feedback);
		feedbackOther.getAnswers().stream().forEach(answer -> {
			answer.setFeedback(feedbackOther);
			logger.info("{}",answer.getFeedback());
			logger.info("{}",answerService.saveOrUpdate(answer));
		});

	}

}

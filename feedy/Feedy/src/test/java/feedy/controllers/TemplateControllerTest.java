package feedy.controllers;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import feedy.FeedyApplication;
import feedy.domain.Question;
import feedy.domain.Template;
import feedy.dtos.QuestionDto;
import feedy.dtos.TemplateDto;
import feedy.services.TemplateService;
import java.util.logging.Level;
import java.util.logging.Logger;

@RunWith(SpringJUnit4ClassRunner.class)

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { FeedyApplication.class })
public class TemplateControllerTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private static final String GET_TEMPLATE_URL = "/templateByName";
	private static final String SAVE_URL = "/saveTemplate";
	private static final Logger LOGGER = Logger.getLogger(UserControllerTest.class.getName());
	private static final String TEMPLATE_NAME_1 = "Template1";
	@Autowired
	private WebApplicationContext context;

	@Autowired
	private TemplateService templateService;

	private MockMvc mockMvc;

	private ObjectWriter ow;

	private String requestJson;

	@Before
	public void setUp() {
		ObjectMapper mapper;
		Set<Question> questions = new TreeSet<>();
		Template template = new Template("Template", "Author", questions);
		Template template1 = new Template(TEMPLATE_NAME_1, "Author1", questions);
		templateService.saveOrUpdate(template);
		templateService.saveOrUpdate(template1);
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		mapper = new ObjectMapper();
		ow = mapper.writer().withDefaultPrettyPrinter();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		requestJson = null;
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void saveTemplateTest() {
		Set<Question> questions = new TreeSet<>();
		Set<QuestionDto> questionsDto = new TreeSet<>();

		Template templateToSave = new Template("TemplateX", "AuthorX", questions);
		int beforeLength = templateService.listAll().size();
		TemplateDto templateDto = new TemplateDto(templateToSave.getName(), templateToSave.getAuthor(), questionsDto);
		try {
			requestJson = ow.writeValueAsString(templateDto);
		} catch (JsonProcessingException e1) {
			LOGGER.log(Level.FINE, "saveTemplateTest writeValueAsString", e1);
		}
		try {
			mockMvc.perform(post(SAVE_URL).contentType(APPLICATION_JSON_UTF8).content(requestJson))
					.andExpect(status().isOk());
			assertTrue(beforeLength < templateService.listAll().size());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "saveTemplateTest", e);
		}
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void saveDuplicateTemplateTest() {
		Set<Question> questions = new TreeSet<>();
		Set<QuestionDto> questionsDto = new TreeSet<>();

		Template templateToSave = new Template(TEMPLATE_NAME_1, "AuthorX", questions);
		int beforeLength = templateService.listAll().size();
		TemplateDto templateDto = new TemplateDto(templateToSave.getName(), templateToSave.getAuthor(), questionsDto);
		try {
			requestJson = ow.writeValueAsString(templateDto);
		} catch (JsonProcessingException e1) {
			LOGGER.log(Level.FINE, "saveDuplicateTemplateTest writeValueAsString", e1);
		}
		try {
			mockMvc.perform(post(SAVE_URL).contentType(APPLICATION_JSON_UTF8).content(requestJson))
					.andExpect(status().isOk());
			assertTrue(beforeLength == templateService.listAll().size());
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "saveDuplicateTemplateTest", e);
		}
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void getTemplateByNameTest() {
		final String TEMPLATE_ONE_NAME = TEMPLATE_NAME_1;

		try {
			mockMvc.perform(get(GET_TEMPLATE_URL + "/" + TEMPLATE_ONE_NAME)).andExpect(status().isOk())
					.andExpect(content().contentType(APPLICATION_JSON_UTF8))
					.andExpect(jsonPath("$.name", is(TEMPLATE_ONE_NAME)));
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "getTemplateByNameTest", e);
		}
	}

	@Test
	@WithMockUser(roles = "SUL")
	public void getTemplateByNameNullTest() {
		final String TEMPLATE_ONE_NAME = "Template2";

		try {
			mockMvc.perform(get(GET_TEMPLATE_URL + "/" + TEMPLATE_ONE_NAME)).andExpect(status().isOk())
					.andExpect(content().contentType(APPLICATION_JSON_UTF8))
					.andExpect(jsonPath("$.name", is(nullValue())));
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "getTemplateByNameNullTest", e);
		}
	}
}

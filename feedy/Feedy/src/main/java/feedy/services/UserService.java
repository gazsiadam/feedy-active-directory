package feedy.services;

import feedy.domain.User;

public interface UserService extends CRUDService<User> {

	User findByName(String name);
}

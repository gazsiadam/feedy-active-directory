package exception;

public class InvalidIdException extends RuntimeException {

	public InvalidIdException(String message) {
		super(message);
	}

	public InvalidIdException(String message, Throwable throwable) {
		super(message, throwable);
	}

}

package feedy.services;

import java.util.Set;

import feedy.domain.Feedback;
import feedy.domain.User;

public interface FeedbackService extends CRUDService<Feedback>{

	public Set<Feedback> findByTo(User to);
	
	public Set<Feedback> findByFrom(User from);

}

package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.QuestionType;

import feedy.repositories.QuestionTypeRepository;

@Service
public class QuestionTypeServiceImpl implements QuestionTypeService {

	private QuestionTypeRepository questionTypeRepository;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	public void setQuestionTypeRepository(QuestionTypeRepository questionTypeRepository) {
		this.questionTypeRepository = questionTypeRepository;
	}

	@Override
	public List<QuestionType> listAll() {
		logger.info("listAll called");

		List<QuestionType> questionTypes = questionTypeRepository.findAll();

		logger.info("listAll result{}", questionTypes);
		return questionTypes;
	}

	@Override
	public QuestionType getById(Integer id) {
		logger.info("getById called {}", id);

		QuestionType questionType = questionTypeRepository.findOne(id);

		logger.info("getById", questionType);
		return questionType;
	}

	@Override
	public QuestionType saveOrUpdate(QuestionType domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return questionTypeRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		questionTypeRepository.delete(id);

	}

	@Override
	public QuestionType findByName(String name) {
		logger.info("findByName called:{}", name);

		QuestionType questionType = questionTypeRepository.findByName(name).get(0);

		logger.info("findByName result:{}", questionType);
		return questionType;
	}

}

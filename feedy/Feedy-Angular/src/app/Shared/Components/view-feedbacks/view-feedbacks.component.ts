import {Component, OnInit} from '@angular/core';
import {Feedback} from '../../Model/feedback';
import {FeedbackService} from '../../Services/feedback.service';
import {LoginService} from '../../Services/login.service';
import {DialogService} from 'ng2-bootstrap-modal';
import {FeedbackModalComponent} from './feedback-modal/feedback-modal.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-view-feedbacks',
  templateUrl: './view-feedbacks.component.html',
  styleUrls: ['./view-feedbacks.component.css']
})
export class ViewFeedbacksComponent  implements OnInit {
  feedbacks: Feedback[] = [];
  received: Boolean = false;

  constructor(private feedBackService: FeedbackService, private loginService: LoginService, private dialogService: DialogService
    , private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.setFeedbacks();

  }

  setFeedbacks() {
    let x: string;
    this.route.queryParams.subscribe(params => {
      x = params['feedbacks'];
      if (x === 'recived') {
        this.received = true;
        console.log('fds');
        this.feedBackService.getFeedbacksRecived(this.loginService.getUser())
          .subscribe(feedbacks => this.feedbacks = feedbacks);
      }
      else {
        console.log('fds1');
        this.feedBackService.getFeedbacksSent(this.loginService.getUser())
          .subscribe(feedbacks => this.feedbacks = feedbacks);
      }
    });
  }


  ViewFeedback(feedback: Feedback) {
    let disposable = this.dialogService.addDialog(FeedbackModalComponent, {
      feedback: feedback
    });
  }

  getReceived() {
    return this.received;
  }
}


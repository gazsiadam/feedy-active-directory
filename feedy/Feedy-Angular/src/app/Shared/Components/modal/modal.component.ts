import {Component, HostListener, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {RoleService} from '../../Services/role.service';
import {TeamService} from '../../Services/team.service';
import {UsersService} from '../../Services/users.service';
import {User} from '../../Model/user';

export interface SaveModal {
  title: string;
  user: User;
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent extends DialogComponent<SaveModal, boolean> implements SaveModal, OnInit {

  title: string;
  user: User;
  username: string;
  password: string;
  selectedTeam: string;
  selectedRole: string;
  teams: string[];
  roles: string[];

  @HostListener('document:keyup', ['$event']) handleKeyUp(event) {
    if (event.keyCode === 27) {
      this.close();
    }
  }
  constructor(dialogService: DialogService,
              private roleService: RoleService, private teamService: TeamService, private userService: UsersService) {
    super(dialogService);
  }

  ngOnInit(): void {
    this.roleService.getRoles().subscribe(roles => this.roles = roles);
    this.teamService.getTeams().subscribe(teams => this.teams = teams);
    if (this.title === 'Edit User') {
      this.save = this.edit;
      this.username = this.user.name;
      this.password = '';
      this.selectedTeam = this.user.team;
      this.selectedRole = this.user.role;
    } else {
      this.save = this.add;
    }
  }



  add() {
    let user = new User(this.username, this.selectedTeam, this.selectedRole, this.password);
    this.userService.saveUser(user).subscribe();
    this.result = true;
    this.close();
    location.reload();
  }

  edit() {
    let user = new User(this.username, this.selectedTeam, this.selectedRole, this.password);
    user.id = this.user.id;
    this.userService.saveUser(user).subscribe();
    this.result = true;
    this.close();
    location.reload();
  }

  save() {

  }

}

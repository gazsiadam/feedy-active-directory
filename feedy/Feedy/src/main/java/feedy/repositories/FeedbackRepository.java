package feedy.repositories;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.Feedback;
import feedy.domain.User;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {

	public Set<Feedback> findByTo(User to);
	
	public Set<Feedback> findByFrom(User from);
}

package feedy.statistics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Answer;
import feedy.domain.Question;
import feedy.services.AnswerService;

@Component
public class QuestionStatistics {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String freeTextQuestionType = "FREETEXT";

	@Autowired
	private AnswerService answerService;

	public Map<String, Long> getTopAnswersForQuestion(Question question) {
		logger.info("getTopAnswers for question:{}", question);

		if (freeTextQuestionType.equals(question.getQuestionType().getName())) {
			logger.info("question type is Freetext no statistics");
			return null;
		}
		List<Answer> answersToQuestion = answerService.listAll().stream().map(a -> (Answer) a)
				.filter(answer -> answer.getQuestion().getId() == question.getId()).collect(Collectors.toList());
		logger.info("{}", answersToQuestion);
		Map<String, Long> mostUsedAnswers = new HashMap<>();
		answersToQuestion.stream().collect(Collectors.groupingBy(Answer::getText, Collectors.counting())).entrySet()
				.stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed()).limit(3)
				.forEach(entry -> mostUsedAnswers.put(entry.getKey(), entry.getValue()));

		logger.info("result:{}", mostUsedAnswers);
		return mostUsedAnswers;
	}
}

import {Component, OnInit, Output} from '@angular/core';
import {User} from '../../Model/user';
import {Team} from '../../Model/team';
import {UsersService} from '../../Services/users.service';
import {ModalComponent} from '../modal/modal.component';
import {DialogService} from 'ng2-bootstrap-modal';
import {LoginService} from '../../Services/login.service';
import {TemplateService} from '../../Services/template.service';

import {UserPipe} from './user.pipe';

import {Template} from '../../Model/template';
import {isNull} from 'util';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-colleagues',
  templateUrl: './colleagues.component.html',
  styleUrls: ['./colleagues.component.css'],
})

export class ColleaguesComponent implements OnInit {
  selectedTemplate: Map<string, number> = new Map();
  listExpanded = false;
  teams: Team[] = [];
  users: User[] = [];
  term: string;
  searchUsersList: User[];

  templates: Template[] = [];
  templateId: number;


  constructor(private usersService: UsersService, private dialogService: DialogService,
              private loginService: LoginService, private templateService: TemplateService, private router: Router) {

    console.log(this.selectedTemplate, this.templateId)
    this.setTemplates();
    this.setTeams();
  }

  changeSelectedTemplateValue(userName, template ) {
    this.selectedTemplate.set(userName, template);
  }

  getRole() {
    return this.loginService.getRole();
  }

  setTeams() {
    this.usersService.getUsers().subscribe(teams => this.teams = teams);

  }
  getUser() {
    return this.loginService.getUser();
  }

  setTemplates() {
    this.templateService.getTemplates().subscribe(templates => this.templates = templates);
  }

  ngOnInit(): void {
  }
  getSelectedTemplate(): number {
    return this.selectedTemplate.entries().next().value[1]
  }
  setTemplateId(username: string) {
    this.templateId = this.getSelectedTemplate();
    let navExtras: NavigationExtras = {
      queryParams: {
        'tempalteID': this.templateId,
        'username': username
      }
    }
    this.router.navigate(['feedback'], navExtras);

}

  edit(user: User) {
    let disposable = this.dialogService.addDialog(ModalComponent, {
      title: 'Edit User',
      user: user
    });
  }

  delete(id: number) {
    this.usersService.deleteUser(id).subscribe();
    location.reload();
  }


}

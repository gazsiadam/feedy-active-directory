package feedy.services;

import feedy.domain.QuestionType;

public interface QuestionTypeService extends CRUDService<QuestionType> {

	QuestionType findByName(String name);

}

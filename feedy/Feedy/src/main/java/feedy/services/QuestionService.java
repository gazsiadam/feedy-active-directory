package feedy.services;

import java.util.List;

import feedy.domain.Question;
import feedy.domain.Template;

public interface QuestionService extends CRUDService<Question> {
	void setTemplate(List<Question> questions, Template template);
}

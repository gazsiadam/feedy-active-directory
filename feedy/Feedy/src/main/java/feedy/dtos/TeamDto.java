package feedy.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TeamDto {
	private String teamName;
	private List<UserDto> users;

	public void addUserToList(UserDto userDto) {
		users.add(userDto);
	}
}

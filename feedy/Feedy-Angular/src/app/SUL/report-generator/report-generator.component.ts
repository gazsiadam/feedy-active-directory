import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Template} from '../../Shared/Model/template';
import {TemplateService} from '../../Shared/Services/template.service';
import {ReportService} from '../../Shared/Services/report.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-report-generator',
  templateUrl: './report-generator.component.html',
  styleUrls: ['./report-generator.component.css']
})
export class ReportGeneratorComponent implements OnInit {
  selectedTemplate: number;
  public myForm: FormGroup;
  public templateTypes: string[];
  public templates: Template[];
  public procentageForTemplate: number;
  public topResultForTemplate: Map<string, Map<string, number>>[] = [];
  public showResult: Boolean = false;
  public templateUsage: Map<string, number> ;
  // PolarArea
  public polarAreaChartData: number[] = [300, 500, 100, 40];
  public polarAreaLegend: boolean = true;
  public polarAreaChartLabels: string[];
  public polarAreaChartType: string = 'polarArea';

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private templateService: TemplateService, private reportService: ReportService, private toastr: ToastrService, private router: Router) {
    this.templateTypes = ['Template Usage', 'Specific Templates'];
    this.setTemplates();
    this.templateUsage = new Map();
    this.getMostUsedTemplates();
    this.setPolarAreaChartLabels();
  }

  ngOnInit() {
  }

  setPolarAreaChartLabels() {

/*    console.log(this.templateUsage);
    this.templateUsage.forEach(item =>
     // this.polarAreaChartLabels.push(item)
      console.log(item)
    );*/
    this.templateUsage.forEach((value: number, key: string) => {
      console.log(key, value);
      this.polarAreaChartLabels.push(key);
    });

    for (let i in this.templateUsage) {
      this.polarAreaChartLabels.push(i);
      console.log('here');
      console.log(i);
    }
    console.log('polarAreaChartLabels');
    console.log(this.polarAreaChartLabels);
  }

  setTemplates() {
    this.templateService.getTemplates().subscribe(templates => this.templates = templates);
  }

  getProcentageForTemplate() {
    this.reportService.GetProcentageForFeedback(this.selectedTemplate).subscribe(procentageForTemplate => {
      console.log(procentageForTemplate);
      this.procentageForTemplate = procentageForTemplate;
    });
    console.log('procentage For Template');
    console.log(this.procentageForTemplate);
  }

  getTopResultForTemplate() {
    this.reportService.GetTopAnswersForTemplateQuestions(this.selectedTemplate).subscribe(topResultForTemplate => {
      console.log(topResultForTemplate);
      topResultForTemplate.forEach(res => {
        let x = new Map();
        Object.keys(res).forEach(key => {
            let y = new Map();
            console.log(res[key]);
            Object.keys(res[key]).forEach(subKey => {
              y.set(subKey, res[key][subKey]);
            });
            x.set(key, y);
          }
        );

        this.topResultForTemplate.push(x);
      });
      console.log(this.topResultForTemplate);
    });
  }

  getMostUsedTemplates() {
    this.reportService.GetTemplatesByUsage().subscribe(data => {
      for (let i in data) {
        console.log(data[i]);
        this.templateUsage.set(i, data[i]);

      }
    });
  }

  resetTopResultForTemplate() {
    this.topResultForTemplate = [];
  }

  setResult() {
    this.resetTopResultForTemplate();
    console.log(this.selectedTemplate);
    this.getProcentageForTemplate();
    this.getTopResultForTemplate();
    this.showResult = true;

  }

}

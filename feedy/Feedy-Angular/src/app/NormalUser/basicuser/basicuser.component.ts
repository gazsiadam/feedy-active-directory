import {LoginService} from '../../Shared/Services/login.service';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-basicuser',
  templateUrl: './basicuser.component.html',
  styleUrls: ['./basicuser.component.css']
})
export class BasicuserComponent implements OnInit {
  userName: string;

  constructor(private loginService: LoginService) {
  }

  ngOnInit() {
    this.setName();
  }

  setName() {
    this.userName = this.loginService.getUser();
    if (this.userName === '') {
      alert('Unauthorized!');
      this.loginService.logout();
    }
  }


  logout() {
    this.loginService.logout();
  }
}

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SuluserComponent} from './suluser.component';

describe('SuluserComponent', () => {
  let component: SuluserComponent;
  let fixture: ComponentFixture<SuluserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SuluserComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuluserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});

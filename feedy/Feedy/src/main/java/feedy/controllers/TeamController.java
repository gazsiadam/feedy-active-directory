package feedy.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feedy.domain.Team;
import feedy.services.TeamService;

@RestController
public class TeamController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TeamService teamService;

	@PreAuthorize("hasAnyRole('Normal','SUL')")
	@RequestMapping(value = "/teams", method = RequestMethod.GET)
	public List<String> getTeams() {
		logger.info("getTeams called");
		List<String> teams = teamService.listAll().stream().map(t -> (Team) t).map(t1 -> t1.getName())
				.collect(Collectors.toList());
		logger.info("getTeams result:{}", teams);
		return teams;
	}
}

 import { NgModule } from '@angular/core';
 import { Routes, RouterModule } from '@angular/router';
 import {TemplateGeneratorComponent} from './SUL/template-generator/template-generator.component';
 import { ReportGeneratorComponent} from './SUL/report-generator/report-generator.component';
 import {ColleaguesComponent} from './Shared/Components/colleagues/colleagues.component';
 import {FeedbackComponent} from './Shared/Components/feedback/feedback.component';
 import {ViewFeedbacksComponent} from './Shared/Components/view-feedbacks/view-feedbacks.component';
import { ViewTemplatesComponent} from './Shared/Components/view-templates/view-templates.component';
 const routes: Routes = [
   {path : 'generate' , component: TemplateGeneratorComponent },
   {path: 'colleagues', component: ColleaguesComponent },
   {path: 'feedback' , component: FeedbackComponent},
   {path: 'Feedbacks' , component: ViewFeedbacksComponent},
   {path: 'templates' , component: ViewTemplatesComponent},
   {path: 'reports' , component: ReportGeneratorComponent}
 ];

 @NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule],
 })
 export class AppRoutingModule { }

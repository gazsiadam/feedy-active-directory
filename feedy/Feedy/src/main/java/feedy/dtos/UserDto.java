package feedy.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDto {

	private int id;
	private String name;
	private String team;
	private String role;
	private String password;

	public UserDto(int id, String name, String team, String role) {
		this.id = id;
		this.name = name;
		this.role = role;
		this.team = team;
		this.password = "dummyPassword";
	}

	public UserDto(String name, String team, String role) {
		this.name = name;
		this.role = role;
		this.team = team;
		this.password = "dummyPassword";
	}

}

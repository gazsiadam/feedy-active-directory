package feedy.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.eclipse.jdt.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
public class Question extends BaseEntity {

	@ManyToOne
	private Template template;
	private String text;

	@OneToOne
	private QuestionType questionType;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<ResponseChoice> responseChoices;
	@OneToMany(cascade = CascadeType.ALL)
	private Set<Answer> answers;

	public Question() {
	}

	public Question(Template template, String text, QuestionType questionType) {

		this.text = text;
		this.questionType = questionType;
		this.template = template;
	}

	public Question(Template template, String text, QuestionType questionType,
			@Nullable Set<ResponseChoice> responseChoices, @Nullable Set<Answer> answers) {
		this.answers = answers;
		this.questionType = questionType;
		this.text = text;
		this.responseChoices = responseChoices;
		this.template = template;
	}

	public Question(String questionText, QuestionType questionType, Set<ResponseChoice> responseChoices) {
		this.questionType = questionType;
		this.text = questionText;
		this.responseChoices = responseChoices;
	}

	public String toString() {
		return "{Question:" + getTemplate() + " " + text + " " + questionType + " " + getId() + "}";
	}

}

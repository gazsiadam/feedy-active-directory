import {Injectable} from '@angular/core';
import {BaseURL} from '../BaseURL';
import {LoginService} from './login.service';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {HeaderBuilder} from '../header-builder';
import {Template} from '../Model/template';
import {Question} from '../Model/question';
import {ResponseChoice} from "../Model/ResponseChoice";


@Injectable()
export class TemplateService {
  private templateURL = BaseURL.getURL() + '/templates';
  private saveTemplateURL = BaseURL.getURL() + '/saveTemplate';
  private templateByNameURL = BaseURL.getURL() + '/getTemplateByName';
  private getURL = BaseURL.getURL() + '/getQuestionsByTemplate';
  private getResponsesURL = BaseURL.getURL() + '/responseChoiceByQuestion';
  private options: RequestOptions;

  constructor(private loginService: LoginService, private http: Http) {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());

  }

  getTemplates(): Observable<Template[]> {
    let options = HeaderBuilder.getOptions(this.loginService.getToken());
    return this.http.get(this.templateURL, options).map(res => res.json() as Template[])
      .catch(err => Observable.throw(err))
  }

  saveTemplate(template: Template): Observable<any> {
    template.author = this.loginService.getUser();
    return this.http.post(this.saveTemplateURL, JSON.stringify(template), this.options).map(res => res.json())
      .catch(err => Observable.throw(err) || 'error');
  }

  // getSelectedTemplate(): Template {
  //   return this.selectedTemplate;
  // }
  //
  // setValueSelectedTemplate(template) {
  //   this.selectedTemplate = template;
  //   console.log(this.selectedTemplate);
  // }

  getTemplateByName(name: string): Observable<Template> {
    let findTemplateUrl = this.templateByNameURL + '/' + name;
    return this.http.get(findTemplateUrl, this.options).map(res => res.json() as Template)
      .catch(err => Observable.throw(err))
  }

  getQuestions(templateId: number): Observable<Question[]> {
    console.log(templateId);
    let getQuestionsURL = this.getURL + '/' + templateId;
    return this.http.get(getQuestionsURL, this.options)
      .map(response => response.json() as Question[]);
  }

  getResponseChoices(questionId: number): Observable<ResponseChoice[]> {
    let getURL = this.getResponsesURL + '/' + questionId;
    return this.http.get(getURL, this.options)
      .map(response => response.json() as ResponseChoice[]);
  }
}


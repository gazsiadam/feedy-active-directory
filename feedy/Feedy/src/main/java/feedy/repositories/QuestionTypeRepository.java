package feedy.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import feedy.domain.QuestionType;

@Repository
public interface QuestionTypeRepository extends JpaRepository<QuestionType, Integer>{
	public List<QuestionType> findByName(String name);
}

package feedy.services;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.ResponseChoice;
import feedy.repositories.ResponseChoiceRepository;

@Service
public class ResponseChoiceServiceImpl implements ResponseChoiceService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	ResponseChoiceRepository responseChoiceRepository;

	@Override
	public List<ResponseChoice> listAll() {
		return Collections.emptyList();
	}

	@Override
	public ResponseChoice getById(Integer id) {
		logger.info("getById called {}", id);

		ResponseChoice responseChoice = responseChoiceRepository.findOne(id);

		logger.info("getById", responseChoice);
		return responseChoice;
	}

	@Override
	public ResponseChoice saveOrUpdate(ResponseChoice domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return responseChoiceRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		responseChoiceRepository.delete(id);
	}

}

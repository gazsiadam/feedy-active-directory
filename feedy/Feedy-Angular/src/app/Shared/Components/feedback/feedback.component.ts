import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../Services/login.service';
import {Question} from '../../Model/question';
import {TemplateService} from '../../Services/template.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ResponseChoice} from '../../Model/ResponseChoice';
import {FormGroup} from '@angular/forms';
import {Answer} from '../../Model/answer';
import {Feedback} from '../../Model/feedback';
import {FeedbackService} from "../../Services/feedback.service";

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  questions: Question[] = [];
  responseChoices: ResponseChoice[] = [];
  answers: any[] = [];
  private sub: any;
  public myForm: FormGroup;
  id: number;
  anonymous: boolean;
  To: string;


  constructor(private loginService: LoginService, private templateService: TemplateService,
              private route: ActivatedRoute, private feedBackService: FeedbackService, private router: Router) {
    this.getQuestions();
  }

  getQuestions() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.id = params['tempalteID'];
      this.To = params['username'];
    });
    this.templateService.getQuestions((this.id)).subscribe(questions => {
      this.questions = questions;
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].questionType === 'MULTIPLE_CHOICE') {
          let x = [];
          for (let j = 0; j < this.questions[i].responseChoices.length; j++) {
            x.push('');
          }
          this.answers[i] = x;
        }
      }
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    let feedback = new Feedback();
    feedback.answers = [];
    for (let i = 0; i < this.questions.length; i++) {
      if (this.questions[i].questionType === 'MULTIPLE_CHOICE') {
        for (let j = 0; j < this.answers[i].length; j++) {
          let answer = new Answer();
          if (this.answers[i][j] === true) {
            answer.text = this.questions[i].responseChoices[j].responseChoiceText;
            console.log(this.answers[i][j]);
            answer.question = this.questions[i];
            feedback.answers.push(answer);
          }

        }
      }
      else {
        let answer = new Answer();
        answer.text = this.answers[i];
        answer.question = this.questions[i];
        feedback.answers.push(answer);
      }
    }
    feedback.from = this.loginService.getUser();
    feedback.to = this.To;
    if (this.anonymous === undefined) {
      feedback.anonymous = false;
    }
    else {
      feedback.anonymous = this.anonymous; }
    feedback.date = Date.now().toLocaleString();
    this.feedBackService.saveFeedback(feedback).subscribe();
    this.router.navigate(['/colleagues']);
  }
}

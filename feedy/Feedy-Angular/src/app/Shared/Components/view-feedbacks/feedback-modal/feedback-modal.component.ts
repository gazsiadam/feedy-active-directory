import {Component, OnInit} from '@angular/core';
import {Feedback} from '../../../Model/feedback';
import {Question} from '../../../Model/question';
import {Answer} from '../../../Model/answer';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {ResponseChoice} from '../../../Model/ResponseChoice';
import * as jsPDF from 'jspdf'
declare var $: any;

export interface FeedbackModal {
  feedback: Feedback;
}

@Component({
  selector: 'app-feedback-modal',
  templateUrl: './feedback-modal.component.html',
  styleUrls: ['./feedback-modal.component.css']
})
export class FeedbackModalComponent extends DialogComponent<FeedbackModal, boolean> implements FeedbackModal, OnInit {
  feedback: Feedback;
  public questions: Question[] = [];

  constructor(dialogService: DialogService) {
    super(dialogService);

  }

  ngOnInit() {
    this.getQuestions();
    console.log(this.feedback);
  }

  formatQuestions(questions: Question[]) {
    for (let i = 0; i < questions.length; i++) {
      questions[i].questionText = questions[i].questionText[0].toUpperCase() + questions[i].questionText.slice(1);
    }
  }

  private getQuestions() {
    let answers = this.feedback.answers;
    for (let i = 0; i < answers.length; i++) {
      if (this.questions.find(question => question.id === answers[i].question.id) === undefined) {
        this.questions.push(answers[i].question);
      }
    }
    this.formatQuestions(this.questions);
  }

  public getAnswersForQuestion(question: Question): Answer[] {
    let result: Answer[] = [];
    this.feedback.answers.forEach(answer => {
      if (answer.question.id === question.id) {
        result.push(answer);
      }
    });
    return result;
  }
  public verifyAnswer(response: ResponseChoice, question: Question): boolean {
    let answers = this.getAnswersForQuestion(question);
    let ok = false;
    answers.forEach(answer => {
      if (answer.text === response.responseChoiceText) { ok = true; }
    });
    return ok;
  }

  download() {
    let doc = new jsPDF();
    doc.fromHTML($('#feedback').get(0), 20, 20, {'width': 500});
    doc.save('Feedback.pdf')
  }
}

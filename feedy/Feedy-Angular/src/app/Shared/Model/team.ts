import {User} from './user';

export class Team {
  teamName: string;
  users: User[];
}

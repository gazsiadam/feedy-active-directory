import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';
import {Team} from '../Model/team';
import {User} from '../Model/user';
import {BaseURL} from '../BaseURL';
import {HeaderBuilder} from '../header-builder';
import {LoginService} from './login.service';

@Injectable()
export class UsersService {

  // Subject to change

  private getURL = BaseURL.getURL() + '/usersByTeam';
  private deleteURL = BaseURL.getURL() + '/deleteUser';
  private saveURL = BaseURL.getURL() + '/saveUser';
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {
    this.options = HeaderBuilder.getOptions(this.loginService.getToken());
  }

  getUsers(): Observable<Team[]> {
    return this.http.get(this.getURL, this.options)
      .map(response => response.json() as Team[]);
  }

  saveUser(user: User): Observable<any> {
    return this.http.post(this.saveURL, JSON.stringify(user), this.options);
  }

  deleteUser(id: number): Observable<any> {
    let deleteUrl = this.deleteURL + '/' + id;
    return this.http.delete(deleteUrl, this.options);
  }


}

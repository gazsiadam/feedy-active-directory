import {LoginService} from '../../Shared/Services/login.service';
import {LoginComponent} from './login/login.component';
import {AfterViewInit, Component, HostListener, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  username: string;
  password: string;

  constructor(private dialogService: DialogService) {
  }

  ngOnInit() {
  }

  showLogin() {
    let disposable = this.dialogService.addDialog(LoginComponent, {});
  }
}

package feedy.converters;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import feedy.domain.Question;
import feedy.domain.QuestionType;
import feedy.domain.ResponseChoice;
import feedy.dtos.QuestionDto;

import feedy.dtos.ResponseChoiceDto;

import feedy.services.QuestionTypeService;

@Component
public class QuestionConverter {


	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ResponseChoiceConverter responseChoiceConverter;

	@Autowired
	private QuestionTypeService questionTypeService;

	public QuestionDto convertToDto(Question question) {
		logger.info("convertToDto:{}",question);
		Set<ResponseChoice> responseChoices = question.getResponseChoices();
		Set<ResponseChoiceDto> responseChoicesDto = new TreeSet<>(Comparator.comparing(ResponseChoiceDto::getId));
		for(ResponseChoice responseChoice : responseChoices) {
			ResponseChoiceDto responseChoiceDto = responseChoiceConverter.convertToDto(responseChoice);
			responseChoicesDto.add(responseChoiceDto);
		}		
		QuestionDto questionDto = new  QuestionDto(question.getId(), question.getText(), question.getQuestionType().getName() ,responseChoicesDto);
		logger.info("convertToDto:{}",questionDto);
		return questionDto;
	}

	public Question convertFromDto(QuestionDto questionDto) {
		logger.info("convertFromDto called:{}", questionDto);

		Set<ResponseChoice> responseChoices = questionDto.getResponseChoices().stream()
				.map(r -> responseChoiceConverter.convertFromDto(r)).collect(Collectors.toSet());

		QuestionType questionType = questionTypeService.findByName(questionDto.getQuestionType());
		Question question = new Question(questionDto.getQuestionText(), questionType, responseChoices);

		logger.info("convertFromDto result:{}", question);
		return question;
	}
}

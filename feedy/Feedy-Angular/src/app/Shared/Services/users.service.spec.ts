import {TestBed, inject} from '@angular/core/testing';
import {
  HttpModule,
  Http,
  Response,
  ResponseOptions,
  BaseRequestOptions,
  XHRBackend
} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {UsersService} from './users.service';

describe('UsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [MockBackend,
        BaseRequestOptions,
        {provide: XHRBackend, useClass: MockBackend},
        UsersService]
    });
  });

  it('should be created', inject([UsersService], (service: UsersService) => {
    expect(service).toBeTruthy();
  }));

  it('should return a list of teams', inject([UsersService, XHRBackend], (service, mockBackend) => {

    const mockResponse = [
      {
        team: "java",
        members: [
          {
            name: "1",
            team: "java",
            role: ""
          },
          {
            name: "2",
            team: "java",
            role: ""
          }

        ]
      },
      {
        team: "dotNet",
        members: [
          {
            name: "1",
            team: "java",
            role: ""
          },
          {
            name: "2",
            team: "java",
            role: ""
          }

        ]

      }
    ];
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(new Response(new ResponseOptions({
        status: 200,
        body: JSON.stringify(mockResponse)
      })));
    });
    service.getUsers().subscribe((users) => {
    });


  }));
});

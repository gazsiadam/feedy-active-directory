package feedy.security.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import feedy.domain.User;
import feedy.repositories.UserRepository;
import feedy.security.other.UserDetailsAdapter;

@Service
public class FeedyUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("loadUserByUsername called: {}", username);
		List<User> user = userRepository.findByUserName(username);
		if (user.isEmpty()) {
			throw new UsernameNotFoundException("No such user");
		}
		UserDetailsAdapter userDetails = new UserDetailsAdapter(user.get(0));

		logger.info("userDetails:{}", userDetails);

		return userDetails;
	}

}

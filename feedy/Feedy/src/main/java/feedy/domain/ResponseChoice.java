package feedy.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class ResponseChoice extends BaseEntity implements Comparable<Object> {

	private String text;

	@ManyToOne(fetch = FetchType.LAZY)
	private Question question;

	public ResponseChoice(String text) {
		this.text = text;
	}

	public String toString() {
		return "{ResponseChoice:" + text + " " + getId() + "}";
	}

	@Override
	public int compareTo(Object o) {
		return this.text.compareTo(((ResponseChoice) o).getText());
	}

	@Override
	public boolean equals(Object obj) {
		return false;

	}

}

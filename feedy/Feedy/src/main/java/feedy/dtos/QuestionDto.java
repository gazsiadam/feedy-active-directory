package feedy.dtos;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QuestionDto {
	private int id;
	private String questionText;
	private String questionType;
	private Set<ResponseChoiceDto> responseChoices;
	
	public void addResponseToQuestion(ResponseChoiceDto responseChoiceDto) {
		responseChoices.add(responseChoiceDto);
	}

}

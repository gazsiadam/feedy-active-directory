package feedy.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedy.domain.Question;
import feedy.domain.Template;
import feedy.repositories.QuestionRepository;

@Service
public class QuestionServiceImpl implements QuestionService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	QuestionRepository questionRepository;

	@Override
	public List<Question> listAll() {
		logger.info("listAll called");

		List<Question> questions = questionRepository.findAll();

		logger.info("listAll result:{}", questions);
		return questions;
	}

	@Override
	public Question getById(Integer id) {
		logger.info("getById called {}", id);

		Question question = questionRepository.findOne(id);

		logger.info("getById", question);
		return question;
	}

	@Override
	public Question saveOrUpdate(Question domainObject) {
		logger.info("saveOrUpdate called {}", domainObject);

		return questionRepository.save(domainObject);
	}

	@Override
	public void delete(Integer id) {
		logger.info("delete called {}", id);

		questionRepository.delete(id);

	}

	@Override
	public void setTemplate(List<Question> questions, Template template) {
		questions.stream().forEach(question -> question.setTemplate(template));

	}

}
